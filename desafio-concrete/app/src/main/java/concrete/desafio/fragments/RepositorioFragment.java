package concrete.desafio.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import concrete.desafio.adapters.RepositorioAdapter;
import concrete.desafio.desafio_concrete.R;
import concrete.desafio.desafio_concrete.RepositorioActivity;
import concrete.desafio.models.repositorios.Item;
import concrete.desafio.models.repositorios.Items;
import concrete.desafio.services.RecyclerViewOnClickListenerHack;

/**
 * Created by rony on 06/10/16.
 */

public class RepositorioFragment extends Fragment implements RecyclerViewOnClickListenerHack{


    private RecyclerView mRecyclerView;
    private List<Item> items;
    private Items itemsRepositorio;


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_repository, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_list);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager llm = (LinearLayoutManager) mRecyclerView.getLayoutManager();
                RepositorioAdapter adapter = (RepositorioAdapter) mRecyclerView.getAdapter();

                if (items.size() == llm.findLastCompletelyVisibleItemPosition() + 1) {
                    List<Item> listAux = null;
                    ((RepositorioActivity) getActivity()).obtemRepositorioGitHub();

                    for (int i = 0; i < listAux.size(); i++) {
                        adapter.addListItem(listAux.get(i), items.size());
                    }
                }
            }
        });


        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(llm);


        RepositorioAdapter adapter = new RepositorioAdapter(getActivity(), itemsRepositorio);


        adapter.setRecyclerViewOnClickListenerHack(this);
        mRecyclerView.setAdapter( adapter );


        return view;
    }

    @Override
    public void onClickListener(View view, int position) {
        Toast.makeText(getActivity(), "Position: "+position, Toast.LENGTH_SHORT).show();

        RepositorioAdapter adapter = (RepositorioAdapter) mRecyclerView.getAdapter();
        adapter.removeListItem(position);
    }
}
