package concrete.desafio.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import concrete.desafio.desafio_concrete.PullRequestActivity;
import concrete.desafio.desafio_concrete.R;
import concrete.desafio.filters.CircleTransform;
import concrete.desafio.holders.RepositorioHolder;
import concrete.desafio.listeners.ItemClickListener;
import concrete.desafio.models.repositorios.Item;
import concrete.desafio.models.repositorios.Items;
import concrete.desafio.services.RecyclerViewOnClickListenerHack;

/**
 * Created by rony on 02/10/16.
 */

public class RepositorioAdapter extends RecyclerView.Adapter<RepositorioHolder>{

    Context c;
    List<Item> items;
    private LayoutInflater mLayoutInflater;

    public RepositorioAdapter(Context context, Items items){
        this.c = context;
        this.items = items.getItems();
        this.mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public RepositorioHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.model_repository, parent, false );
        RepositorioHolder repoHolder = new RepositorioHolder(view);
        return repoHolder;
    }

    @Override
    public void onBindViewHolder(RepositorioHolder repoHolder, int position) {
        repoHolder.nomeTxt.setText(items.get(position).getFullName());
        repoHolder.repoDescricao.setText(items.get(position).getDescription());
        repoHolder.nickname.setText(items.get(position).getOwner().getLogin());
        repoHolder.numStars.setText(String.valueOf(items.get(position).getWatchersCount()));
        repoHolder.numFork.setText( String.valueOf(items.get(position).getForksCount()));
        repoHolder.repoName.setText(items.get(position).getName());

        Picasso.with(c)
                .load( items.get(position).getOwner().getAvatarUrl() )
                .transform(new CircleTransform())
                .into(repoHolder.img);

        //ClickListener
        repoHolder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                Snackbar.make(v, items.get(pos).getFullName(), Snackbar.LENGTH_SHORT).setAction("Action", null).show();

                Intent pullRequestActivity = new Intent(v.getContext(), PullRequestActivity.class);
                pullRequestActivity.putExtra("owner", items.get(pos).getOwner().getLogin());
                pullRequestActivity.putExtra("reponame", items.get(pos).getName());
                pullRequestActivity.putExtra("closed", String.valueOf(items.get(pos).getForksCount()));
                pullRequestActivity.putExtra("open_issues", String.valueOf(items.get(pos).getOpenIssuesCount()));
                v.getContext().startActivity(pullRequestActivity);

            }
        });

    }

    public void addListItem(Item c, int position){
        items.add(c);
        notifyItemInserted(position);
    }


    public void removeListItem(int position){
        items.remove(position);
        notifyItemRemoved(position);
    }

    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack r){
        RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack = r;
    }

    @Override
    public int getItemCount() {

        return items.size();
    }
}
