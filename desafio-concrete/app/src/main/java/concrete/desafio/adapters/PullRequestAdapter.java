package concrete.desafio.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import concrete.desafio.desafio_concrete.PullRequestActivity;
import concrete.desafio.desafio_concrete.R;
import concrete.desafio.filters.CircleTransform;
import concrete.desafio.holders.RepositorioHolder;
import concrete.desafio.listeners.ItemClickListener;
import concrete.desafio.models.pullrequests.PullRequestsRepositorio;
import concrete.desafio.models.repositorios.Item;
import concrete.desafio.models.repositorios.Items;

/**
 * Created by rony on 02/10/16.
 */

public class PullRequestAdapter extends RecyclerView.Adapter<RepositorioHolder>{

    Context c;
    List<PullRequestsRepositorio> pulls;
    private LayoutInflater mLayoutInflater;

    public PullRequestAdapter(Context context, List<PullRequestsRepositorio> pulls){
        this.c = context;
        this.pulls = pulls;
        this.mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public RepositorioHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.model_pull_request, parent, false );
        RepositorioHolder repoHolder = new RepositorioHolder(view);
        return repoHolder;
    }

    @Override
    public void onBindViewHolder(RepositorioHolder repoHolder, int position) {
        repoHolder.nomeTxt.setText(pulls.get(position).getUser().getType());
        repoHolder.repoDescricao.setText(pulls.get(position).getBody());
        repoHolder.nickname.setText(pulls.get(position).getUser().getLogin());
        repoHolder.repoName.setText(pulls.get(position).getTitle());

        Picasso.with(c)
                .load( pulls.get(position).getUser().getAvatarUrl() )
                .transform(new CircleTransform())
                .into(repoHolder.img);

        repoHolder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                Snackbar.make(v, "Exibindo repositório " + pulls.get(pos).getTitle(), Snackbar.LENGTH_SHORT).setAction("Action", null).show();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pulls.get(pos).getHtmlUrl()));
                v.getContext().startActivity(browserIntent);

            }
        });

    }


    @Override
    public int getItemCount() {

        return pulls.size();
    }
}
