package concrete.desafio.listeners;

import android.view.View;

/**
 * Created by rony on 02/10/16.
 */

public interface ItemClickListener {

    void onItemClick(View v, int pos);
}
