package concrete.desafio.services;

import java.util.List;

import concrete.desafio.models.pullrequests.PullRequestsRepositorio;
import concrete.desafio.models.repositorios.Items;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by rony on 01/10/16.
 */

public interface GitHubService {

    String GIT_BASE_URL = "https://api.github.com/";

    @GET("search/repositories?q=language:Java&sort=stars&page=1")
    Call<Items> listRepos();


    @GET("repos/{owner}/{reponame}/pulls")
    Call<List<PullRequestsRepositorio>> listPullRequestsRepo(@Path("owner") String owner, @Path("reponame") String reponame);

    class Factory{

        public static GitHubService service;

        public static GitHubService getInstance(){
            if(service == null){
                Retrofit retrofit = new Retrofit.Builder()
                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl(GIT_BASE_URL)
                        .build();

                service = retrofit.create(GitHubService.class);

            }
            return service;
        }
    }

}
