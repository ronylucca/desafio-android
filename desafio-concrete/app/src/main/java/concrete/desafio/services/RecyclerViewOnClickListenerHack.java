package concrete.desafio.services;

import android.view.View;

/**
 * Created by rony on 01/10/16.
 */
public interface RecyclerViewOnClickListenerHack {
    public void onClickListener(View view, int position);
}
