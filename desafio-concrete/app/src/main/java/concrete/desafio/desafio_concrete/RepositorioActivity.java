package concrete.desafio.desafio_concrete;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import concrete.desafio.adapters.RepositorioAdapter;
import concrete.desafio.models.repositorios.Items;
import concrete.desafio.services.GitHubService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RepositorioActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    TextView texto;
    Items items = new Items();
    Context context = this;
    RecyclerView rv;
    SearchView mSearchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSearchView = (SearchView) findViewById(R.id.mSearch);
        rv = (RecyclerView) findViewById(R.id.myRecycler);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setItemAnimator(new DefaultItemAnimator());
        FloatingActionButton floatingBtn = (FloatingActionButton) findViewById(R.id.floatingBtn);
        floatingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Solicitando Fork do Respositório", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();


            }
        });

        obtemRepositorioGitHub();

    }


    public void obtemRepositorioGitHub(){

        GitHubService.Factory.getInstance().listRepos().enqueue(new Callback<Items>() {
            @Override
            public void onResponse(Call<Items> call, Response<Items> response) {
                Log.i( "Sucesso", String.valueOf(response.body().getItems().size()));
                items = response.body();
                RepositorioAdapter repositorioAdapter = new RepositorioAdapter(context, items);
                rv.setAdapter(repositorioAdapter);

            }

            @Override
            public void onFailure(Call<Items> call, Throwable t) {
                Log.e( "Falhou", t.getMessage());

            }
        });

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.i("QUERY", query);
        //obtemRepositorioGitHub(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}
