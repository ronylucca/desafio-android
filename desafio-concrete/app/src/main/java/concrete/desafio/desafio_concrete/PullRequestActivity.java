package concrete.desafio.desafio_concrete;

import android.app.ActionBar;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import concrete.desafio.adapters.PullRequestAdapter;
import concrete.desafio.models.pullrequests.PullRequestsRepositorio;
import concrete.desafio.services.GitHubService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rony on 04/10/16.
 */
public class PullRequestActivity extends AppCompatActivity {


    TextView texto;
    List<PullRequestsRepositorio> pulls = new ArrayList<PullRequestsRepositorio>();
    Context context = this;
    RecyclerView rv;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String owner = getIntent().getStringExtra("owner");
        String reponame = getIntent().getStringExtra("reponame");
        String closed = getIntent().getStringExtra("closed");
        String open_issues = getIntent().getStringExtra("open_issues");

        setContentView(R.layout.activity_pull_request);

        setTitle(reponame);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        myToolbar.setTitle(open_issues + " opened / " + closed + " closed");
        myToolbar.setActivated(true);
        rv = (RecyclerView) findViewById(R.id.recyclerPullRequest);

        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setItemAnimator(new DefaultItemAnimator());


        obtemPullRepositorioGitHub(owner, reponame);

    }



    public void obtemPullRepositorioGitHub(String owner, String reponame){

        Log.i("Iniciando chamada", "obtemPullRepositorioGitHub() ");
        GitHubService.Factory.getInstance().listPullRequestsRepo(owner, reponame).enqueue(new Callback <List<PullRequestsRepositorio>>() {

            @Override
            public void onResponse(Call<List<PullRequestsRepositorio>> call, Response<List<PullRequestsRepositorio>> response) {

                    Log.i( "Sucesso", String.valueOf(response.body().size()));

                pulls = response.body();
                PullRequestAdapter repositorioAdapter = new PullRequestAdapter(context, pulls);
                rv.setAdapter(repositorioAdapter);

            }

            @Override
            public void onFailure(Call<List<PullRequestsRepositorio>> call, Throwable t) {
                Log.e( "Falhou", t.getMessage());

            }
        });

    }

    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
