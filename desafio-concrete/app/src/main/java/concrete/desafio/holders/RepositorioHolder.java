package concrete.desafio.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import concrete.desafio.desafio_concrete.R;
import concrete.desafio.listeners.ItemClickListener;

/**
 * Created by rony on 02/10/16.
 */

public class RepositorioHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public ImageView img;
    public TextView nomeTxt, nickname, repoDescricao, repoName;
    public ItemClickListener itemClickListener;
    public TextView numStars, numFork;

    public RepositorioHolder(View itemView) {
        super(itemView);
        this.img = (ImageView) itemView.findViewById(R.id.repoUserImage);
        this.nomeTxt = (TextView) itemView.findViewById(R.id.userFullName);
        this.nickname = (TextView) itemView.findViewById(R.id.userNickNameTxt);
        this.repoDescricao = (TextView) itemView.findViewById(R.id.repoDescricao);
        this.numFork = (TextView) itemView.findViewById(R.id.numForksRepo);
        this.numStars = (TextView) itemView.findViewById(R.id.numStarsRepo);
        this.repoName = (TextView) itemView.findViewById(R.id.repoNomeTxt);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        this.itemClickListener.onItemClick(view, getAdapterPosition());
    }



    public void setItemClickListener(ItemClickListener ic){
        this.itemClickListener = ic;
    }
}
